/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.message.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.jxmpp.jid.BareJid;

import java.util.Date;
import java.util.UUID;

import static org.project_uplink.projectuplink.persistence.message.entity.MessageEntity.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class MessageEntity {

    public static final String TABLE_NAME = "messages";

    @PrimaryKey
    private UUID id;
    private Date date;
    private BareJid sender;
    private BareJid recipient;
    private String content;

    public MessageEntity(@NonNull UUID id, @NonNull Date date, @NonNull BareJid sender, @NonNull BareJid recipient, String content) {
        this.id = id;
        this.date = date;
        this.sender = sender;
        this.recipient = recipient;
        this.content = content;
    }

    public UUID getId() {
        return id;
    }

    public void setId(@NonNull UUID id) {
        this.id = id;
    }

    public BareJid getSender() {
        return sender;
    }

    public void setSender(@NonNull BareJid sender) {
        this.sender = sender;
    }

    public BareJid getRecipient() {
        return recipient;
    }

    public void setRecipient(@NonNull BareJid recipient) {
        this.recipient = recipient;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
