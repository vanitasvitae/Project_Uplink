/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.roster.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import org.project_uplink.projectuplink.persistence.BaseDao;
import org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterGroupEntity;

import java.util.List;

import static org.project_uplink.projectuplink.persistence.roster.entity.RosterGroupEntity.COL_NAME;
import static org.project_uplink.projectuplink.persistence.roster.entity.RosterGroupEntity.TABLE;

@Dao
public interface RosterGroupDao extends BaseDao<RosterGroupEntity> {

    @Query("SELECT * FROM " + TABLE)
    List<RosterGroupEntity> getAll();

    @Query("SELECT * FROM " + TABLE + " WHERE " + COL_NAME + " LIKE :groupName")
    RosterGroupEntity getByGroupName(String groupName);

    @Query("SELECT " + RosterEntryEntity.T_COL_JID + ", " + RosterEntryEntity.T_COL_NICK +
            " FROM " + RosterEntryEntity.TABLE +
            " INNER JOIN " + MembershipEntity.TABLE +
            " ON " + RosterEntryEntity.T_COL_JID + '=' + MembershipEntity.T_COL_JID +
            " WHERE " + MembershipEntity.T_COL_GROUP + " LIKE :groupName")
    List<RosterEntryEntity> getMembersOf(String groupName);

    @Query("SELECT COUNT(*) FROM " + TABLE)
    int count();
}
