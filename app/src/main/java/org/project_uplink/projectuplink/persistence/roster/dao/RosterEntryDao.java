/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.roster.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import org.jxmpp.jid.BareJid;
import org.project_uplink.projectuplink.persistence.BaseDao;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity;

import java.util.List;

import static org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity.COL_JID;
import static org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity.COL_NICK;
import static org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity.TABLE;

@Dao
public abstract class RosterEntryDao implements BaseDao<RosterEntryEntity> {

    @Query("SELECT * FROM " + TABLE)
    public abstract List<RosterEntryEntity> getAll();

    @Query("SELECT * FROM " + TABLE)
    public abstract LiveData<List<RosterEntryEntity>> getAllLive();

    @Query("SELECT * FROM " + TABLE + " WHERE " + COL_JID + " LIKE :jid")
    public abstract RosterEntryEntity getByJid(BareJid jid);

    @Query("SELECT * FROM " + TABLE + " WHERE " + COL_NICK + " LIKE :nick")
    public abstract RosterEntryEntity getByNick(String nick);

    @Query("DELETE FROM " + TABLE)
    public abstract void deleteAll();

    @Query("DELETE FROM " + TABLE + " WHERE " + COL_JID + " IN :jids")
    public abstract void deleteByJids(BareJid... jids);

    @Query("DELETE FROM " + TABLE + " WHERE " + COL_JID + " IN :jids")
    public abstract void deleteByJids(List<BareJid> jids);

    @Transaction
    public void update(List<RosterEntryEntity> entries) {
        deleteAll();
        for (RosterEntryEntity e : entries) {
            insert(e);
        }
    }

    @Query("SELECT COUNT(*) FROM " + TABLE)
    public abstract int count();
}
