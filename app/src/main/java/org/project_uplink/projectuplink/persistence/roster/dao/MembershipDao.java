/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.roster.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import org.jxmpp.jid.BareJid;
import org.project_uplink.projectuplink.persistence.BaseDao;
import org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity;

import java.util.List;

import static org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity.COL_GROUP;
import static org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity.COL_JID;
import static org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity.TABLE;

@Dao
public interface MembershipDao extends BaseDao<MembershipEntity> {

    @Query("SELECT * FROM " + TABLE)
    List<MembershipEntity> getAll();

    @Query("SELECT * FROM " + TABLE + " WHERE " + COL_JID + " LIKE :jid")
    List<MembershipEntity> getByJid(BareJid jid);

    @Query("SELECT * FROM " + TABLE + " WHERE " + COL_GROUP + " LIKE :groupName")
    List<MembershipEntity> getByName(String groupName);

    @Query("SELECT COUNT(*) FROM " + TABLE)
    int count();
}
