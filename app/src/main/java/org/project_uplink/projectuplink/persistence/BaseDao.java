/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * DAO with basic functionality.
 * @see <a href="https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1">7 Pro-tips for room - Florina Muntenescu</a>
 */
public interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<T> entities);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T... entities);

    @Delete
    void delete(T... entities);

    @Delete
    void delete(List<T> entities);

    @Update
    void update(T... entities);

    @Update
    void update(List<T> entities);
}
