/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import org.project_uplink.projectuplink.persistence.roster.dao.MembershipDao;
import org.project_uplink.projectuplink.persistence.roster.dao.RosterEntryDao;
import org.project_uplink.projectuplink.persistence.roster.dao.RosterGroupDao;
import org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterGroupEntity;
import org.project_uplink.projectuplink.persistence.type_converters.Converters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {
        RosterEntryEntity.class,
        RosterGroupEntity.class,
        MembershipEntity.class
},
        version = 1)
@TypeConverters({Converters.class})
public abstract class UplinkDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "uplinkdatabase.db";

    private static volatile UplinkDatabase INSTANCE;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    public abstract RosterEntryDao rosterEntries();
    public abstract RosterGroupDao rosterGroups();
    public abstract MembershipDao rosterGroupMemberships();

    public static synchronized UplinkDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = createInstance(context);
        }
        return INSTANCE;
    }

    private static UplinkDatabase createInstance(Context context) {
        return Room.databaseBuilder(
                context.getApplicationContext(),
                UplinkDatabase.class,
                DATABASE_NAME)
                .build();
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    public void execute(Runnable runnable) {
        executor.submit(runnable);
    }
}
