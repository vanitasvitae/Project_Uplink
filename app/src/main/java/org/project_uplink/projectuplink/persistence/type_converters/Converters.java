/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.type_converters;

import android.arch.persistence.room.TypeConverter;
import android.support.annotation.NonNull;

import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.FullJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.Date;
import java.util.UUID;

public class Converters {

    @TypeConverter
    public static BareJid bareJidFromString(String jid) {
        try {
            return JidCreate.bareFrom(jid);
        } catch (XmppStringprepException e) {
            throw new AssertionError(e);
        }
    }

    @TypeConverter
    public static String bareJidToString(BareJid jid) {
        return jid.toString();
    }

    @TypeConverter
    public static FullJid fullJidFromString(String jid) {
        try {
            return JidCreate.fullFrom(jid);
        } catch (XmppStringprepException e) {
            throw new AssertionError(e);
        }
    }

    @TypeConverter
    public String fullJidToString(FullJid jid) {
        return jid.toString();
    }

    @TypeConverter
    public static UUID uuidFromString(String string) {
        return UUID.fromString(string);
    }

    @TypeConverter
    public static String uuidToString(UUID uuid) {
        return uuid.toString();
    }

    @TypeConverter
    public static Date dateFromLong(@NonNull Long lonk) {
        return new Date(lonk);
    }

    public static Long dateToLong(Date date) {
        return date.getTime();
    }
}
