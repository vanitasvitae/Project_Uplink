/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.roster.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.jxmpp.jid.BareJid;

import static org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity.TABLE;

@Entity(tableName = TABLE)
public class RosterEntryEntity {

    public static final String TABLE = "rosterentries";
    public static final String COL_JID = "jid";
    public static final String COL_NICK = "nickname";

    public static final String T_COL_JID = TABLE + '.' + COL_JID;
    public static final String T_COL_NICK = TABLE + '.' + COL_NICK;

    @NonNull
    @PrimaryKey
    private BareJid jid;

    private String nickname;

    public RosterEntryEntity(@NonNull BareJid jid, String nickname) {
        this.jid = jid;
        this.nickname = nickname;
    }

    @NonNull
    public BareJid getJid() {
        return jid;
    }

    public void setJid(@NonNull BareJid jid) {
        this.jid = jid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
