/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.roster.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.support.annotation.NonNull;

import org.jxmpp.jid.BareJid;

import static android.arch.persistence.room.ForeignKey.CASCADE;
import static org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity.COL_GROUP;
import static org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity.COL_JID;
import static org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity.TABLE;

@Entity(tableName = TABLE,
        primaryKeys = {COL_JID, COL_GROUP},
        foreignKeys = {
                @ForeignKey(entity = RosterEntryEntity.class,
                        parentColumns = RosterEntryEntity.COL_JID,
                        childColumns = COL_JID,
                        onDelete = CASCADE),
                @ForeignKey(entity = RosterGroupEntity.class,
                        parentColumns = RosterGroupEntity.COL_NAME,
                        childColumns = COL_GROUP,
                        onDelete = CASCADE)})
public class MembershipEntity {

    public static final String TABLE = "rostergroup_memberships";
    public static final String COL_JID = "jid";
    public static final String COL_GROUP = "groupName";

    public static final String T_COL_JID = TABLE + '.' + COL_JID;
    public static final String T_COL_GROUP = TABLE + '.' + COL_GROUP;

    @NonNull
    @ColumnInfo(name = COL_GROUP)
    private String groupName;

    @NonNull
    @ColumnInfo(name = COL_JID)
    private BareJid jid;

    public MembershipEntity(@NonNull BareJid jid, @NonNull String groupName) {
        this.jid = jid;
        this.groupName = groupName;
    }

    @NonNull
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(@NonNull String groupName) {
        this.groupName = groupName;
    }

    @NonNull
    public BareJid getJid() {
        return jid;
    }

    public void setJid(@NonNull BareJid jid) {
        this.jid = jid;
    }
}
