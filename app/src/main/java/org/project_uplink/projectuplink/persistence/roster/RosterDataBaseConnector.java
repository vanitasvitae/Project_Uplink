/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence.roster;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterGroup;
import org.jivesoftware.smack.roster.RosterListener;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.Jid;
import org.project_uplink.projectuplink.persistence.UplinkDatabase;
import org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterGroupEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RosterDataBaseConnector implements RosterListener {

    private final Roster roster;
    private final UplinkDatabase database;

    public RosterDataBaseConnector(Roster roster, UplinkDatabase database) {
        this.roster = roster;
        this.database = database;
    }

    @Override
    public void entriesAdded(final Collection<Jid> addresses) {
        entriesUpdated(addresses);
    }

    @Override
    public void entriesUpdated(final Collection<Jid> addresses) {
        this.database.execute(new Runnable() {
            @Override
            public void run() {
                for (Jid j : addresses) {
                    RosterEntry entry = roster.getEntry(j.asBareJid());
                    RosterEntryEntity entity = new RosterEntryEntity(entry.getJid(), entry.getName());
                    database.rosterEntries().insert(entity);

                    // Delete old memberships
                    List<MembershipEntity> memberships = database.rosterGroupMemberships().getByJid(entity.getJid());
                    database.rosterGroupMemberships().delete(memberships);

                    // Insert new memberships
                    List<RosterGroup> groups = entry.getGroups();
                    memberships.clear();
                    for (RosterGroup g : groups) {
                        RosterGroupEntity grentity = new RosterGroupEntity(g.getName());
                        database.rosterGroups().insert(grentity);
                        memberships.add(new MembershipEntity(entity.getJid(), grentity.getGroupName()));
                    }
                    database.rosterGroupMemberships().insert(memberships);
                }
            }
        });
    }

    @Override
    public void entriesDeleted(final Collection<Jid> addresses) {
        final List<BareJid> jids = new ArrayList<>();
        for (Jid j : addresses) {
            jids.add(j.asBareJid());
        }
        this.database.execute(new Runnable() {
            @Override
            public void run() {
                database.rosterEntries().deleteByJids(jids);
            }
        });
    }

    @Override
    public void presenceChanged(Presence presence) {

    }
}
