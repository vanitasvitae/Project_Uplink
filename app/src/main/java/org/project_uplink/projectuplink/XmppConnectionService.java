/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.SubscribeListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.carbons.CarbonManager;
import org.jivesoftware.smackx.csi.ClientStateIndicationManager;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.project_uplink.projectuplink.persistence.UplinkDatabase;
import org.project_uplink.projectuplink.persistence.roster.RosterDataBaseConnector;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity;

import java.io.IOException;
import java.net.InetAddress;
import java.util.List;
import java.util.Locale;

public class XmppConnectionService extends Service
        implements IncomingChatMessageListener, ConnectionListener, PingFailedListener {

    public static final String TAG = UplinkApplication.TAG;

    private static final String PREFIX = "org.project-uplink.uplink";

    public static final String ACTION_START = PREFIX + ".START_FOREGROUND";
    public static final String ACTION_STOP = PREFIX + ".STOP_FOREGROUND";
    public static final String ACTION_CONNECT = PREFIX + ".CONNECT";
    public static final String ACTION_DISCONNECT = PREFIX + ".DISCONNECT";
    public static final String ACTION_PING = PREFIX + ".PING";

    public static final String EXTRA_JID = PREFIX + ".EXTRA_JID";




    public static final int NOTIFICATION_ID_FOREGROUND = 314159; // Prime + nearly 100.000 x Pi. Neat!
    public static final String NOTIFICATION_CHANNEL_FOREGROUND = "org.project-uplink.uplink.CHANNEL.FOREGROUND";

    public static final int NOTIFICATION_ID_DEBUG = 110110;
    public static final String NOTIFICATION_CHANNEL_DEBUG = "org.project-uplink.uplink.CHANNEL.DEBUG";

    private XMPPConnection connection;
    private UplinkDatabase database;
    Handler handler = new Handler(Looper.getMainLooper());
    private ConnectionStatus connectionStatus = ConnectionStatus.disconnected;

    public XmppConnectionService() {
        super();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.database = UplinkDatabase.getInstance(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        this.stopConnection();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action == null) action = "";
        switch (action) {
            case ACTION_START:
                Log.d(TAG, "Start foreground service.");
                showForegroundNotification();
                createConnection();
                break;

            case ACTION_STOP:
                Log.d(TAG, "Stop foreground service.");
                stopConnection();
                stopForeground(true);
                stopSelf();
                break;
            default:
                Log.d(TAG, "Unknown action: \"" + action + "\"");
        }
        return START_STICKY_COMPATIBILITY;
    }

    public synchronized void createConnection() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String jidString = preferences.getString(SettingsActivity.PREF_JID, null);
        final String password = preferences.getString(SettingsActivity.PREF_PASSWORD, null);
        final String resource = preferences.getString(SettingsActivity.PREF_RESOURCE, null);

        if (jidString == null || password == null) {
            return;
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "Attempt login.");
                    BareJid jid = JidCreate.entityBareFrom(jidString);
                    InetAddress address = InetAddress.getByName(jid.getDomain().toString());
                    XMPPTCPConnectionConfiguration configuration = XMPPTCPConnectionConfiguration.builder()
                            .setResource((resource != null && !resource.isEmpty()) ? resource : "Uplink")
                            .setXmppDomain(jid.asDomainBareJid())
                            .setUsernameAndPassword(jid.getLocalpartOrThrow(), password)
                            .setHostAddress(address)
                            .setConnectTimeout(2 * 60 * 1000)
                            .build();

                    XMPPTCPConnection xmpptcpConnection = new XMPPTCPConnection(configuration);
                    xmpptcpConnection.addConnectionListener(XmppConnectionService.this);

                    ReconnectionManager.getInstanceFor(xmpptcpConnection)
                            .enableAutomaticReconnection();

                    Roster roster = Roster.getInstanceFor(xmpptcpConnection);
                    roster.addRosterListener(new RosterDataBaseConnector(roster, database));

                    connection = xmpptcpConnection.connect();
                    xmpptcpConnection.login();

                    PingManager.getInstanceFor(connection).setPingInterval(5 * 60);
                    PingManager.getInstanceFor(connection).registerPingFailedListener(XmppConnectionService.this);

                    if (ClientStateIndicationManager.isSupported(connection)) {
                        ClientStateIndicationManager.inactive(connection);
                    }

                    ChatManager.getInstanceFor(connection).addIncomingListener(XmppConnectionService.this);

                    CarbonManager carbonManager = CarbonManager.getInstanceFor(connection);
                    if (carbonManager.isSupportedByServer()) {
                        carbonManager.enableCarbons();
                    }

                    Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.accept_all);
                    Roster.getInstanceFor(connection).addSubscribeListener(new SubscribeListener() {
                        @Override
                        public SubscribeAnswer processSubscribe(Jid from, Presence subscribeRequest) {
                            return SubscribeAnswer.ApproveAndAlsoRequestIfRequired;
                        }
                    });

                } catch (InterruptedException | SmackException | IOException | XMPPException e) {
                    Log.e(TAG, "Error starting connection:", e);
                }
            }
        });
        thread.start();
    }

    public synchronized void stopConnection() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (connection != null) {
                    ((XMPPTCPConnection) connection).disconnect();
                    connection = null;
                }
            }
        });
        thread.start();
    }

    public synchronized void restartConnection() {
        Log.d(TAG, "Restarting connection.");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ((XMPPTCPConnection) connection).instantShutdown();
                createConnection();
            }
        });
        thread.start();
    }

    private void showForegroundNotification() {
        Intent startSettingsActivityIntent = new Intent(this, SettingsActivity.class);

        PendingIntent settingsIntent = PendingIntent.getActivity(this, 0,
                startSettingsActivityIntent, 0);

        Intent stopServiceIntent = new Intent(this, XmppConnectionService.class);
        stopServiceIntent.setAction(ACTION_STOP);

        PendingIntent stopIntent = PendingIntent.getService(this, 0, stopServiceIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_FOREGROUND)
                .setSmallIcon(R.drawable.ic_phonelink_black_24dp)
                .addAction(R.drawable.ic_settings_black_24dp,
                        getResources().getString(R.string.settings), settingsIntent)
                .addAction(R.drawable.ic_settings_black_24dp, getResources().getString(R.string.stop), stopIntent)
                .setContentIntent(settingsIntent)
                .setOngoing(true)
                .build();
        startForeground(NOTIFICATION_ID_FOREGROUND, notification);
    }

    @Override
    public void newIncomingMessage(EntityBareJid from, final Message message, final Chat chat) {
        RosterEntry entry = Roster.getInstanceFor(connection).getEntry(from.asBareJid());
        if (entry == null) return;

        String body = message.getBody();
        if (body == null) {
            return;
        }
        if (body.startsWith("/open ")) {
            String link = message.getBody().substring("/open ".length());
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(link));
            startActivity(i);
            return;
        } else if (body.startsWith("/battery")) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
                int level = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                try {
                    chat.send("Battery is at " + level + "%.");
                } catch (SmackException.NotConnectedException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return;
        } else if (body.startsWith("/xep")) {
            String nr = body.substring("/xep ".length());
            if (nr.length() > 4) {
                return;
            }

            int xep_nr;
            try {
                xep_nr = Integer.parseInt(nr);
            } catch (NumberFormatException e) {
                Log.d(TAG, "Can't parse number " + nr, e);
                return;
            }
            String link = "https://xmpp.org/extensions/xep-" + String.format(Locale.US, "%04d", xep_nr) + ".html";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(link));
            startActivity(i);
            return;
        } else if (body.startsWith("/roster")) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    List<RosterEntryEntity> entries = database.rosterEntries().getAll();
                    StringBuilder out = new StringBuilder("Roster:\n");
                    for (RosterEntryEntity e : entries) {
                        out.append(e.getNickname()).append(": ").append(e.getJid()).append("\n");
                    }
                    try {
                        chat.send(out.toString());
                    } catch (SmackException.NotConnectedException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(XmppConnectionService.this, message.getBody(), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void connected(XMPPConnection connection) {
        Log.d(TAG, "XmppConnectionService.connected()");
        connectionStatus = ConnectionStatus.connected;
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        Log.d(TAG, "XmppConnectionService.authenticated(): Resumed: " + resumed);
    }

    @Override
    public void connectionClosed() {
        Log.d(TAG, "XmppConnectionService.connectionClosed()");
        connectionStatus = ConnectionStatus.disconnected;
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        Log.d(TAG, "XmppConnectionService.connectionClosedOnError()", e);
        showErrorNotification("connectionClosedOnError", e);
        connectionStatus = ConnectionStatus.disconnected;
    }

    @Override
    public void reconnectionSuccessful() {
        Log.d(TAG, "XmppConnectionService.reconnectionSuccessful()");
        connectionStatus = ConnectionStatus.connected;
    }

    @Override
    public void reconnectingIn(int seconds) {
        Log.d(TAG, "XmppConnectionService.reconnectingIn(): " + seconds + " seconds.");
        connectionStatus = ConnectionStatus.connecting;
    }

    @Override
    public void reconnectionFailed(Exception e) {
        Log.d(TAG, "XmppConnectionService.reconnectionFailed()", e);
        showErrorNotification("reconnectionFailed", e);
        connectionStatus = ConnectionStatus.disconnected;
    }

    private void showErrorNotification(String title, Exception e) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_DEBUG)
                .setSmallIcon(R.drawable.ic_phonelink_black_24dp)
                .setContentTitle(title)
                .setContentText(e != null ? e.toString() : null)
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(NOTIFICATION_ID_DEBUG, mBuilder.build());
    }

    @Override
    public void pingFailed() {
        Log.d(TAG, "Ping failed.");
        restartConnection();
    }
}
