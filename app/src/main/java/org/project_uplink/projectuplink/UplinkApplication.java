/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import org.jivesoftware.smack.SmackConfiguration;

import static org.project_uplink.projectuplink.XmppConnectionService.NOTIFICATION_CHANNEL_DEBUG;
import static org.project_uplink.projectuplink.XmppConnectionService.NOTIFICATION_CHANNEL_FOREGROUND;

public class UplinkApplication extends Application {

    public static final String TAG = "Uplink";

    public UplinkApplication() {
        super();
        SmackConfiguration.DEBUG = true;
    }

    public void onCreate() {
        super.onCreate();
        initializeNotificationChannels(getApplicationContext());
        Intent serviceIntent = new Intent(getApplicationContext(), XmppConnectionService.class);
        serviceIntent.setAction(XmppConnectionService.ACTION_START);
        if (Build.VERSION.SDK_INT < 26) {
            startService(serviceIntent);
        } else {
            startForegroundService(serviceIntent);
        }
    }

    public void initializeNotificationChannels(Context context) {
        // Only necessary on Android O and upwards.
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Foreground notification channel
        String fName = getResources().getString(R.string.channel_name_foreground);
        String fDescription = getResources().getString(R.string.channel_description_foreground);

        NotificationChannel foreground = new NotificationChannel(NOTIFICATION_CHANNEL_FOREGROUND,
                fName, NotificationManager.IMPORTANCE_DEFAULT);
        foreground.setDescription(fDescription);
        foreground.setImportance(NotificationManager.IMPORTANCE_MIN);
        foreground.setShowBadge(false);
        notificationManager.createNotificationChannel(foreground);

        // Debug notification channel
        String dName = "DEBUG";
        String dDescription = "Debug Messages";
        NotificationChannel debug = new NotificationChannel(NOTIFICATION_CHANNEL_DEBUG,
                dName, NotificationManager.IMPORTANCE_HIGH);
        debug.setDescription(dDescription);
        debug.setShowBadge(false);
        debug.enableVibration(true);
        notificationManager.createNotificationChannel(debug);
    }
}
