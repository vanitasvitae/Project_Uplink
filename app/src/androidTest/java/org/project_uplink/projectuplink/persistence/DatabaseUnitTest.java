/*
 * Copyright 2018 Paul Schaub
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.project_uplink.projectuplink.persistence;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;
import org.project_uplink.projectuplink.persistence.roster.dao.RosterEntryDao;
import org.project_uplink.projectuplink.persistence.roster.dao.RosterGroupDao;
import org.project_uplink.projectuplink.persistence.roster.dao.MembershipDao;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterEntryEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.RosterGroupEntity;
import org.project_uplink.projectuplink.persistence.roster.entity.MembershipEntity;

import java.io.IOException;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.TestCase.assertEquals;

@RunWith(AndroidJUnit4.class)
public class DatabaseUnitTest {

    private UplinkDatabase db;
    private BareJid alice;

    @Before
    public void createDb() throws XmppStringprepException {
        Context context = InstrumentationRegistry.getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context, UplinkDatabase.class).build();
        alice = JidCreate.bareFrom("alice@wonderland.lit");
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void rosterEntryTest() throws XmppStringprepException {
        RosterEntryDao dao = db.rosterEntries();
        assertEquals(0, dao.count());

        RosterEntryEntity a = new RosterEntryEntity(alice, "Alice");
        assertEquals("Alice", a.getNickname());
        dao.insert(a);
        assertEquals(1, dao.count());


        RosterEntryEntity a_ = dao.getByJid(alice);
        assertNotNull(a_);
        assertEquals(a.getNickname(), a_.getNickname());

        List<RosterEntryEntity> list = dao.getAll();
        assertEquals(1, list.size());

        dao.delete(a_);
        assertEquals(0, dao.count());
    }

    @Test
    public void rosterGroupTest() throws XmppStringprepException {
        RosterEntryDao eDao = db.rosterEntries();
        RosterGroupDao gDao = db.rosterGroups();
        MembershipDao mDao = db.rosterGroupMemberships();

        RosterGroupEntity group = new RosterGroupEntity("Test");
        assertEquals(0, gDao.count());
        gDao.insert(group);
        assertEquals(1, gDao.count());

        RosterEntryEntity entry = new RosterEntryEntity(alice, "Alice");
        eDao.insert(entry);

        MembershipEntity member = new MembershipEntity(alice, "Test");
        assertEquals(0, mDao.count());
        mDao.insert(member);
        assertEquals(1, mDao.count());
        assertEquals(1, mDao.getByJid(alice).size());

        eDao.delete(entry);
        assertEquals(0, mDao.count());
        assertEquals(1, gDao.count());

        eDao.insert(entry);
        mDao.insert(member);
        gDao.delete(group);
        assertEquals(0, mDao.count());
        assertEquals(1, eDao.count());


        gDao.insert(group);
        mDao.insert(member);
        mDao.insert(new MembershipEntity(alice, "Test"));
        assertEquals(1, mDao.count());
    }

    @Test(expected = SQLiteConstraintException.class)
    public void missingConstraintsFailsTest() throws XmppStringprepException {
        MembershipDao mDao = db.rosterGroupMemberships();
        BareJid jid = JidCreate.bareFrom("invalid@abc.de");
        MembershipEntity membership = new MembershipEntity(jid, "invalid");
        mDao.insert(membership);
    }
}



